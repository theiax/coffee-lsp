Create a new folder for the xtext server and navigate to that folder.

Generate the xtext project using Yeoman. The installation guide is :
https://blogs.itemis.com/en/get-your-first-web-enabled-dsl-running-in-60-seconds

npm install -g yo generator-xtext 
yo xtext

To run the project, we have to be inside the xtext directory we created and has to execute : `./gradlew jettyRun`

Let's say the project package is `org.lbtc.automation.home`. `automation` is the umbrella folder. `home` is the subdomain. `Automata` is the language name and `atma` is the extensioon of the DSL.The DSL grammar file will be present at `org.lbtc.automation.home/src/main/java/org/lbtc/automation/home/Automata.xtext`.

Then we can proceed with xtext designing using the below link:

https://www.eclipse.org/Xtext/documentation/102_domainmodelwalkthrough.html#create-a-new-xtext-project


**More resources:**

https://www.typefox.io/blog/domain-specific-languages-in-theia-and-vs-code

https://www.typefox.io/blog/xtext-lsp-vs-xtext-web


